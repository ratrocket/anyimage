package anyimage

import (
	"errors"
	"image"
	"image/gif"
	"image/jpeg"
	"image/png"
	"io"
	"strings"
)

// Encode takes an image and a hint (which can be a filename or an
// extension) and writes the image m to w in the image format hinted at.
func Encode(w io.Writer, m image.Image, hint string) error {
	s := strings.ToLower(hint)

	if strings.HasSuffix(s, "jpg") || strings.HasSuffix(s, "jpeg") {
		err := jpeg.Encode(w, m, nil)
		if err != nil {
			return err
		}
	} else if strings.HasSuffix(s, "gif") {
		err := gif.Encode(w, m, nil)
		if err != nil {
			return err
		}
	} else if strings.HasSuffix(s, "png") {
		err := png.Encode(w, m)
		if err != nil {
			return err
		}
	} else {
		// unknown format
		return errors.New("anyimage.Encode: unknown image format")
	}
	return nil
}
