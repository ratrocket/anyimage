> **(May 2021)** moved to [md0.org/anyimage](https://md0.org/anyimage). Use `import md0.org/anyimage`

Super simple function to write an image.Image to jpeg, png, or gif.

Like having a polymorphic image.Encode function.
